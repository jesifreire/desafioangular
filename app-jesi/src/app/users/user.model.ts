
    export interface User {
        id: number;
        nome: string;
        email: string;
        login: string;
        admin: boolean;
    }

   export interface ResponseUser{
       data: User[];
   }

   export interface RequestCreate {
    email: string;
    login: string;
    nome: string;
    senha: string;
}
export interface ResponseCreate {
        email: string;
        login: string;
        nome: string;
        senha: string;
        createdAt: Date;
    }
//Modelo para o get User

export interface ResponseUser {
    data: User[];
}
//Modelos para o update User
export interface requestUpdate {
    admin: boolean;
    email: string;
    id: number;
    login: string;
    nome: string;
    senha: string;
}
export interface ResponseUpdate {
    admin: boolean;
    email: string;
    id: number;
    login: string;
    nome: string;
    senha: string;
    createdAt: Date;
}
