import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { Observable } from 'rxjs';
import { RequestCreate, requestUpdate, ResponseCreate, ResponseUpdate, ResponseUser } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = "https://combustivelapp.herokuapp.com/api/usuarios";

  constructor(private http: HttpClient) { }

  getUsers(): Observable<ResponseUser> {
    return this.http.get<ResponseUser>(this.url);
   
  }
  createUser(request: RequestCreate): Observable<ResponseCreate> {
    return this.http.post<ResponseCreate>(this.url, request)
  }
  getUser(id: string): Observable<ResponseUser> {
    const _url  = `${this.url}/${id}`;

    return this.http.get<ResponseUser>(_url);
  }
  updateUser(id: string, request: requestUpdate): Observable<ResponseUpdate> {
    const _url  = `${this.url}/${id}`;
    return this.http.put<ResponseUpdate>(_url, request)
  }
}
 